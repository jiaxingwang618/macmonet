import React, { useState }  from 'react'
import { StyleSheet, TextInput, Text, View, Button, ScrollView, FlatList, TouchableOpacity, Image, ImageBackground, KeyboardAvoidingView } from 'react-native';
import { PrimaryBlue, PrimaryBlueOpacity } from '../color'
import LogoImage from '../components/LogoImage';
import Spinner from 'react-native-loading-spinner-overlay';

const LogInScreen = ({navigation, route}) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [loading, setLoading] = useState(false)
    const signInHandler = () => {
        console.log("sign, email: ", email, " password: ", password);
        // Check validation show loading
        setLoading(true)
        setTimeout(() => {
            setLoading(false)
            navigation.navigate(`${route.params.role} Home Page`)
        }, 2000)
        // if true => navigation.navigate('homepage')
        // else => stay, pop error message
    }
    const emailChangeHandler = (enteredEmail) => {
        setEmail(enteredEmail)
        
    }
    const passwordChangeHandler = (enteredPassword) => {
        setPassword(enteredPassword)
    }

    
    return (
        <View style={styles.screenContainer}>
            <Spinner
                visible={loading}
                overlayColor={'rgba(0,0,0,0.5)'}
                textContent={'loading...'}
                textStyle={styles.spinnerTextStyle}
            />
            <View style={styles.contentContainer} >
                <View style={styles.logoContainer}>
                    
                </View>
                <View style={styles.titleContainer}>
                    <LogoImage />
                    <Text style={styles.textDis}>{route.params.role} Sign In</Text>
                </View>
                <KeyboardAvoidingView style={styles.inputContainer} behavior="padding" >
                    <TextInput 
                    placeholder="Email" 
                    keyboardType='email-address' 
                    textContentType='emailAddress'
                    autoCorrect={false} 
                    autoCompleteType='email' 
                    autoCapitalize="none" 
                    placeholderTextColor='rgba(255,255,255,0.7)' 
                    onChangeText={emailChangeHandler} 
                    value={email} style={styles.textInput} />
                    <TextInput 
                    placeholder="Password" 
                    autoCorrect={false} 
                    autoCompleteType='password' 
                    secureTextEntry={true}
                    textContentType="password"
                    autoCapitalize="none" 
                    placeholderTextColor='rgba(255,255,255,0.7)' 
                    onChangeText={passwordChangeHandler} 
                    value={password} 
                    style={styles.textInput} />
                </KeyboardAvoidingView>
                <View style={styles.signInContainer}>
                    <TouchableOpacity onPress={signInHandler}>
                        <View style={styles.signInBtn}>
                            <Text style={styles.btnText}>SIGN IN</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.secondaryBtnContainer}>
                    <TouchableOpacity onPress={()=> navigation.goBack()}>
                        <View style={styles.secondaryBtn}>
                            <Text style={styles.secondaryBtnText}>Change Role</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=> navigation.navigate('Forgot Password', { role: route.params.role})}>
                        <View style={styles.secondaryBtn}>
                            <Text style={styles.secondaryBtnText}>Forgot password?</Text>
                        </View>
                    </TouchableOpacity>
                
                </View>
                <View style={styles.registerBtnContainer}>
                    <Text style={{color: 'white'}}>Not a member yet?</Text>
                    <TouchableOpacity onPress={()=> navigation.navigate('Register', { role: route.params.role})}>
                        <View style={styles.registerBtn}>
                            <Text style={styles.registerBtnText}>REGISTER</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
            
        </View>
    )
}

const styles = StyleSheet.create({
    screenContainer: {
        flex: 1,
        paddingTop: 50,
        backgroundColor: PrimaryBlue
    },
    backgroundImageWrapper:{
        flex: 1,
    },
    contentContainer: {
        flex: 1,
        justifyContent: "space-between",
        alignItems: 'center',
        // borderWidth: 1,
        // borderColor: 'black'
    },
    titleContainer: {
        flex: 3,
        // borderWidth: 1,
        // borderColor: 'black',
        justifyContent: 'space-around',
        alignItems: "center"
    },
    textDis: {
        textAlign: 'center',
        color: "white",
        fontSize: 24
    },
    inputContainer: {
        flex: 1,
        width: '80%',
        justifyContent: "space-around",
        alignItems: "center",
        // borderWidth: 1,
        // borderColor: 'black',
        
    },
    textInput: { 
        width: "80%", 
        padding: 10,
        textAlign: "center",
        borderBottomColor: 'rgba(255,255,255,0.3)',
        borderBottomWidth: 1,
        // borderWidth: 1,
        // borderColor: 'black',
    },
    signInContainer: {
        flex: 1,
        width: '80%',
        // borderWidth: 1,
        // borderColor: 'black',
        justifyContent: 'flex-end'
    },
    signInBtn: {
        backgroundColor: 'white',
        borderRadius: 5,
        height: 50,
        width: '100%',
        justifyContent: 'center',
    },
    secondaryBtnContainer: {
        flex: 1,
        width: '80%',
        marginTop: 10,
        flexDirection: "row",
        justifyContent: "space-between",
        // borderWidth: 1,
        // borderColor: 'black',
    },
    secondaryBtn: {
        
    },
    secondaryBtnText: {
        textAlign: "center",
        color: 'rgba(255,255,255,0.6)'
    },
    registerBtnContainer: {
        // borderColor: 'black',
        // borderWidth: 1,
        alignItems: "center",
        flex: 2,
        width: "80%",
        justifyContent:'center'
    },
    registerBtn: {
        backgroundColor: 'rgba(0,0,0,0)',
        borderRadius: 5,
        height: 50,
        width: 100,
        justifyContent: 'center',
    },
    registerBtnText:{
        textAlign: "center",
        color: 'white'
    },

    btnText: {
        textAlign: "center",
        color: PrimaryBlue
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
})

export default LogInScreen