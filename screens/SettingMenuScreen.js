import React from "react"
import { View, Text, StyleSheet, TouchableOpacity} from "react-native"
import { MoreInfoScreenStyleSheet as styles} from '../apiHelper/predefinedStyleSheet'
import TitleHeader from "../components/TitleHeader"


const SettingMenuScreen = () => {
    return (
        <View style={{...styles.screenContainer, ...localStyle.screenContainer}} >
            <TitleHeader title="Settings" />
            <View style={localStyle.BtnList}>
                <TouchableOpacity style={localStyle.settingBtn} >
                    <View style={localStyle.settingBtnView}>
                        <Text style={localStyle.settingBtnText}>Account Setting</Text>    
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={localStyle.settingBtn}>
                    <View style={localStyle.settingBtnView}>
                        <Text style={localStyle.settingBtnText}>Log Out</Text>                    
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default SettingMenuScreen

const localStyle = StyleSheet.create({
    screenContainer: {
        backgroundColor: 'white'
    },
    BtnList: {
        flex: 10,
        borderTopWidth: 1,
        borderColor: '#bbbbbb'

    },
    settingBtn: {
        borderBottomWidth: 1,
        borderColor: '#bbbbbb',
        height: 50,
        justifyContent: "center",
        alignItems: "center"
    },
    settingBtnView: {
    },
    settingBtnText: {
        textAlign: 'center',
        fontSize: 18
    }
})