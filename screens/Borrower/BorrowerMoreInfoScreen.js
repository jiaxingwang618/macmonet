import React, { useState, useEffect }  from 'react'
import { StyleSheet, TextInput, Text, View, ScrollView, TouchableOpacity,Button, Platform, Modal, Alert, KeyboardAvoidingView } from 'react-native';
import { PrimaryBlue, PrimaryBlueOpacity } from '../../color'
import LogoImage from '../../components/LogoImage';
import { onlyNumber } from '../../apiHelper/helper';
import Spinner from 'react-native-loading-spinner-overlay';
import { MoreInfoScreenStyleSheet as styles} from '../../apiHelper/predefinedStyleSheet'

const BorrowerMoreInfoScreen = ({navigation, route}) => {
    const {email, password, dob, name, roleSelect} = route.params
    

    const [creditScore, setCreditScore] = useState('')
    const [sin, setSin] = useState('')
    const [loanHad, setLoanHad] = useState('')
    const [totalLoan, setTotalLoan] = useState('')
    const [agreementOpen, setagreementOpen] = useState(false)
    const [haveAgreed, setHaveAgreed] = useState(false)
    const [loading, setLoading] = useState(false)


    const registerHandler = () => {
        // check agreement agreed
        if(!haveAgreed) {return Alert.alert(
            'Agreement not agreed',
            'In order to continue, you must agree to our Term of Service',
            [
                {text: 'OK', onPress: () => {console.log("ok")}}
            ],
            {cancelable: false}
        )}
        
        console.log("sign, email: ", email, " password: ", password, "role:", roleSelect, "SIN:", sin);
        // Check validation
        setLoading(true)
        setTimeout(()=> {
            setLoading(false)
            navigation.navigate("Select Role")
        }, 2000)
        // try register
        // if true => navigation.navigate('sign in page')
        // navigation.navigate("Select Role")
        // else => stay, pop error message
    }

    const creditScoreChangeHandler = (score) => {
        setCreditScore(onlyNumber(score))   
    }

    const sinChangeHandler = (sin) => {
        setSin(onlyNumber(sin))   
    }

    const loanHadChangeHandler = (loanHad) => {
        setLoanHad(onlyNumber(loanHad))   
    }

    const totalLoanChangeHandler = (totalLoan) => {
        setTotalLoan(onlyNumber(totalLoan))
    }

    const toggleAgreementOpen = () => {
        // open agreement modal
        setagreementOpen(open => !open)
    }

    const iAgreeHandler = () => {
        toggleAgreementOpen()
        setHaveAgreed(agree => !agree)
    }

    return (
        <KeyboardAvoidingView style={styles.screenContainer} behavior="padding">
            <Spinner
                visible={loading}
                overlayColor={'rgba(0,0,0,0.5)'}
                textContent={'loading...'}
                textStyle={styles.spinnerTextStyle}
            />
            <View style={styles.contentContainer} >
                <View style={styles.titleContainer}>
                    <LogoImage />
                    <Text style={styles.textDis}>Borrower More Info</Text>
                </View>
                <View style={styles.inputContainer}>
                    <TextInput 
                    placeholder="Equifax credit score" 
                    autoCorrect={false} 
                    placeholderTextColor='rgba(255,255,255,0.7)' 
                    onChangeText={creditScoreChangeHandler} 
                    value={creditScore}
                    keyboardType="number-pad"
                    returnKeyType={ 'done' }
                    style={styles.textInput} />

                    <TextInput 
                    placeholder="SIN" 
                    keyboardType='number-pad' 
                    autoCorrect={false} 
                    autoCapitalize="none" 
                    placeholderTextColor='rgba(255,255,255,0.7)' 
                    onChangeText={sinChangeHandler} 
                    returnKeyType={ 'done' }
                    value={sin} style={styles.textInput} />
                    
                    <TextInput 
                    placeholder="Loan had already (CAD$)" 
                    keyboardType='number-pad' 
                    autoCorrect={false} 
                    autoCapitalize="none" 
                    placeholderTextColor='rgba(255,255,255,0.7)' 
                    onChangeText={loanHadChangeHandler} 
                    returnKeyType={ 'done' }
                    value={loanHad} style={styles.textInput} />

                    <TextInput 
                    placeholder="Total Loan Needed (CAD$)" 
                    keyboardType='number-pad' 
                    autoCorrect={false}  
                    autoCapitalize="none" 
                    placeholderTextColor='rgba(255,255,255,0.7)' 
                    returnKeyType={ 'done' }
                    onChangeText={totalLoanChangeHandler} 
                    value={totalLoan} 
                    style={styles.textInput} />
                </View>
                <View style={styles.agreementContainer}>
                    <Text>
                        {haveAgreed ? 
                        <Text style={{fontSize: 20, color: 'green', fontWeight: "bold"}}>✓</Text> : 
                        <Text style={{fontSize: 20, color: 'red', fontWeight: "bold"}}>☐</Text> }
                    </Text>
                    <TouchableOpacity onPress={toggleAgreementOpen}>
                        <View style={styles.agreementOpenBtn}>
                            <Text style={styles.agreementBtnText}>Please read and agree the contract</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.registerContainer}>
                    <TouchableOpacity onPress={registerHandler}>
                        <View style={styles.registerBtn}>
                            <Text style={styles.btnText}>REGISTER</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
            
            <Modal visible={agreementOpen} animationType='slide'>
                <View style={styles.agreementModelContainer}> 
                    <ScrollView style={styles.agreementContentContainer}>
                        <Text>
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                        </Text>
                    </ScrollView>
                    <View style={styles.agreementBtnContainer}>
                        <TouchableOpacity onPress={toggleAgreementOpen}>
                            <View style={styles.agreementBtn}>
                                <Text style={styles.btnText}>Back</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={iAgreeHandler}>
                            <View style={styles.agreementBtn}>
                            <Text style={styles.btnText}>{haveAgreed? 'I Disagree' : 'I Agree'}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
            </KeyboardAvoidingView>
    )
}

export default BorrowerMoreInfoScreen