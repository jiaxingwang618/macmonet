import React, { useState, useEffect }  from 'react'
import { StyleSheet, TextInput, Text, Image, View, TouchableOpacity,KeyboardAvoidingView  } from 'react-native';
import LogoImage from '../../components/LogoImage';
import { onlyNumber } from '../../apiHelper/helper';
import Spinner from 'react-native-loading-spinner-overlay';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import { MoreInfoScreenStyleSheet as styles} from '../../apiHelper/predefinedStyleSheet'


const AccurateLoanScreen = ({navigation}) => {
    const [email, setEmail] = useState('')
    const [sin, setSin] = useState('')
    const [address, setAddress] = useState('')
    const [loading, setLoading] = useState(false)
    const [image, setImage] = useState(null)
    const [uploading, setUploading] = useState('Click Here To Upload ID')

    useEffect(()=> {
        getPermissionAsync();
        console.log('hi');
    })

    const _pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.All,
          allowsEditing: true,
          base64: true,
          quality: 1
        });
        
        console.log(result);
        setUploading('uploading...')
        if (!result.cancelled) {
          setImage(result.uri);
          setUploading('uploaded')
          return
        }
        setUploading('Click Here To Upload ID')
    }

    const getPermissionAsync = async () => {
        if (Constants.platform.ios) {
          const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
          if (status !== 'granted') {
            alert('Sorry, we need camera roll permissions to make this work!');
          }
        }
      }

    const accurateEstimateHandler = () => {
        
        console.log("sign, email: ", email, " password: ", password, "role:", roleSelect);
        // Check validation
        setLoading(true)
        setTimeout(()=> {
            setLoading(false)
            navigation.navigate("Select Role")
        }, 2000)
        // try register
        // if true => navigation.navigate('sign in page')
        // navigation.navigate("Select Role")
        // else => stay, pop error message
    }

    const sinChangeHandler = (sin) => {
        setSin(onlyNumber(sin))   
    }

    const emailChangeHandler = (enteredEmail) => {
        setEmail(enteredEmail)   
    }

    const addressChangeHandler = (address) => {
        setAddress(address)   
    }

    

    return (
        <KeyboardAvoidingView style={{...styles.screenContainer,...localStyle.contentContainer}} behavior="padding" >
            <Spinner
                visible={loading}
                overlayColor={'rgba(0,0,0,0.5)'}
                textContent={'loading...'}
                textStyle={styles.spinnerTextStyle}
            />
            <View style={styles.contentContainer} >
                <View style={{...styles.titleContainer, ...localStyle.titleContainer}}>
                    <LogoImage style={localStyle.logoImage} height={35} width={35} />
                    <Text style={localStyle.titleText}>Your Loan Credit</Text>
                </View>
                <View style={styles.inputContainer}>
                    <TextInput 
                    placeholder="SIN" 
                    keyboardType='number-pad' 
                    autoCorrect={false} 
                    autoCapitalize="none" 
                    placeholderTextColor='rgba(255,255,255,0.7)' 
                    onChangeText={sinChangeHandler} 
                    returnKeyType={ 'done' }
                    value={sin} style={styles.textInput} />
                    
                    <TextInput 
                    placeholder="Street Address" 
                    autoCorrect={false} 
                    autoCompleteType="street-address"
                    autoCapitalize="none" 
                    placeholderTextColor='rgba(255,255,255,0.7)' 
                    onChangeText={addressChangeHandler} 
                    returnKeyType={ 'done' }
                    value={address} style={styles.textInput} />

                    <TextInput 
                    placeholder="Email" 
                    keyboardType='email-address' 
                    textContentType='emailAddress'
                    autoCorrect={false} 
                    autoCompleteType='email' 
                    autoCapitalize="none" 
                    placeholderTextColor='rgba(255,255,255,0.7)' 
                    onChangeText={emailChangeHandler} 
                    value={email} style={styles.textInput} />

                    <TouchableOpacity onPress={_pickImage} style={styles.dateInput} >
                        <Text 
                        editable={false}
                        autoCorrect={false} 
                        placeholderTextColor='rgba(255,255,255,0.7)' 
                        style={localStyle.uploadText}>{uploading}</ Text>
                    </TouchableOpacity>
                    {image &&<View style={localStyle.imageContainer}>
                        <Image source={{ uri: image }} style={{ width: 100, height: 100 }} />
                    </View> }
                </View>
                <View style={{...styles.registerContainer, ...localStyle.btnContainer}}>
                    <TouchableOpacity style={localStyle.btn} onPress={accurateEstimateHandler}>
                        <View style={styles.registerBtn}>
                            <Text style={styles.btnText}>Get Accurate Loan Estimate</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
            
        </KeyboardAvoidingView>
    )
}

const localStyle = StyleSheet.create({
    contentContainer: {
        justifyContent: 'flex-start'
    },
    titleContainer: {
        flex: 0.4,
        flexDirection: 'row',
        justifyContent: 'center',
        // borderColor: 'black',
        // borderWidth: 1
    },
    logoImage: {
        marginRight: 20
    },
    titleText: {
        fontSize: 25
    },
    imageContainer: {
        marginVertical: 20
    },
    btnContainer: {
        flex: 0.5,
        justifyContent: 'flex-end'
    },
    btn: {
        marginBottom: '10%'
    },
    uploadText: {
        color: 'white'
    }
})

export default AccurateLoanScreen