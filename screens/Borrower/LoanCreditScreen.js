import React, { useState } from 'react'
import { View, Text, StyleSheet, TouchableOpacity} from 'react-native'
import { MoreInfoScreenStyleSheet as styles} from '../../apiHelper/predefinedStyleSheet'
import LogoImage from '../../components/LogoImage'
import TitleHeader from '../../components/TitleHeader'

const LoanCreditScreen = ({navigation, route}) => {
    const [avaibleCredit, setAvaiableCredit] = useState('4,000.05')

    return (
        <View style={{...styles.screenContainer, ...localStyle.screenContainer}}>
            <TitleHeader title="Your Loan Credit" />
            <View style={{...styles.card, ...localStyle.creditDisp}}>
                <Text style={localStyle.creditText}>$ {avaibleCredit}</Text>
            </View>
            <View style={localStyle.interestContainer}>
                <Text style={localStyle.mainText}>Interest Range</Text>
                <Text style={localStyle.subText} >8.22 ~ 12.33 / Yearly</Text>
            </View>
            <View style={localStyle.releaseContainer}>
                <Text style={localStyle.mainText}>Release Date</Text>
                <Text style={localStyle.subText}>2020-02-16</Text>
            </View>
            <View style={localStyle.btnContainer}>
                <TouchableOpacity onPress={()=> {navigation.navigate('Accurate Estimate')}}>
                    <View style={localStyle.accurateBtn}>
                        <Text style={localStyle.accurateBtnText}>Get more accurate estimation -></Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const localStyle = StyleSheet.create({
    screenContainer: {
        backgroundColor: 'white',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    titleContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        // borderColor: 'black',
        // borderWidth: 1
    },
    logoImage: {
        marginRight: 20
    },
    titleText: {
        fontSize: 25
    },
    creditDisp: {
        flex: 2,
        borderRadius: 10,
        textAlignVertical: 'center',
        alignItems: "center",
        justifyContent: 'center'
        // borderColor: 'black',
        // borderWidth: 1
    },
    creditText: {
        fontSize: 45,
        textAlign: 'center',
        textAlignVertical:'center'
    },
    interestContainer: {
        flex: 1.2,
        width: '90%',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'red',
        borderRadius: 15,
        borderWidth: 3,
        marginTop: 30
    },
    releaseContainer: {
        flex: 1.2,
        width: '90%',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'red',
        borderRadius: 15,
        borderWidth: 3,
        marginTop: 30
    },
    mainText: {
        fontSize: 25,
        marginVertical: 10
    },
    subText: {
        fontSize: 15,
        color: '#777777'
    },
    btnContainer: {
        flex: 2,
        justifyContent: 'center'
    },
    accurateBtn: {
        borderRadius: 600,
        backgroundColor: 'orange',
        padding: 20,
        paddingHorizontal: 30
    },
    accurateBtnText: {
        color: 'white',
        fontWeight: 'bold'
    }
})


export default LoanCreditScreen