import React from 'react'
import { createDrawerNavigator } from '@react-navigation/drawer';
import SettingMenuScreen from '../SettingMenuScreen';
import AccurateLoanScreen from './AccurateLoanScreen';
import LoanCreditScreen from './LoanCreditScreen';


const Drawer = createDrawerNavigator();

const BorrowerDrawer = props => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Loan Credit" component={LoanCreditScreen} />
      <Drawer.Screen name="Accurate Estimate" component={AccurateLoanScreen} />
      <Drawer.Screen name="Setting Menu" component={SettingMenuScreen} />
    </Drawer.Navigator>
  );
}

export default BorrowerDrawer