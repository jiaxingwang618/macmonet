import React from 'react'
import { createDrawerNavigator } from '@react-navigation/drawer';
import SettingMenuScreen from '../SettingMenuScreen';
import BrokerTabs from './BrokerTabs';

const Drawer = createDrawerNavigator();

const BrokerDrawer = props => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Explore List" component={BrokerTabs} />
      <Drawer.Screen name="Setting Menu" component={SettingMenuScreen} />
    </Drawer.Navigator>
  );
}

export default BrokerDrawer


