import React from 'react'
import {View, Text, StyleSheet} from 'react-native'
import {BrokerListScreenStyleSheet as styles} from '../../apiHelper/predefinedStyleSheet'
import TitleHeader from '../../components/TitleHeader'
import BrokerList from '../../components/BrokerList'

const BrokerBorrowerListScreen = (props) => {
    return (
        <View style={styles.screenContainer}>
            <TitleHeader style={localStyle.titleContainer} title="Borrowers"/>
            <BrokerList />
            <View style={localStyle.premTextContainer} >
                <Text style={localStyle.premText} >
                    There are 9473 Borrowers in Total,
                    Become Our Premium Account to view all
                </Text>    
            </View>
        </View>
    )
}

export default BrokerBorrowerListScreen

const localStyle = StyleSheet.create({
    titleContainer: {
        // borderColor: 'black',
        // borderWidth: 1,
        flex: 1
    },
    premTextContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 30
    },
    premText: {
        color: '#777777',
        
    },

})