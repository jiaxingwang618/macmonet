import React from 'react'
import {View, Text} from 'react-native'
import {BrokerListScreenStyleSheet as styles} from '../../apiHelper/predefinedStyleSheet'
import TitleHeader from '../../components/TitleHeader'
import BrokerList from '../../components/BrokerList'


const BrokerOrderListScreen = (props) => {
    return (
        <View style={styles.screenContainer}>
            <TitleHeader title="Orders"/>
            <BrokerList />

        </View>
    )
}

export default BrokerOrderListScreen