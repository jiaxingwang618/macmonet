import React from 'react'
import { StyleSheet, TextInput, Text, View, ScrollView, TouchableOpacity,Button, Platform, Modal, KeyboardAvoidingView} from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'
import BrokerLenderListScreen from './BrokerLenderListScreen'
import BrokerBorrowerListScreen from './BrokerBorrowerListScreen'
import BrokerOrderListScreen from './BrokerOrderListScreen'


const iosTab = createBottomTabNavigator();
const androidTab = createMaterialBottomTabNavigator();

const BrokerTabs = ({navigation, route}) => {

    if(Platform.OS === 'ios'){
        return (
            <iosTab.Navigator>
                <iosTab.Screen name="Lender List" component={BrokerLenderListScreen}
                options={{
                    tabBarLabel: 'Lenders'
                }} 
                />
                <iosTab.Screen name="Borrower List" component={BrokerBorrowerListScreen} 
                options={{
                    tabBarLabel: 'Borrowers'
                }} 
                />
                <iosTab.Screen name="Order List" component={BrokerOrderListScreen} 
                options={{
                    tabBarLabel: 'Orders'
                }} 
                />
            </iosTab.Navigator>
        )
    }else{
        return (
            <androidTab.Navigator>
                <androidTab.Screen name="Lender List" component={BrokerLenderListScreen}
                options={{
                    tabBarLabel: 'Lenders'
                }} 
                />
                <androidTab.Screen name="Borrower List" component={BrokerBorrowerListScreen} 
                options={{
                    tabBarLabel: 'Borrowers'
                }} 
                />
                <androidTab.Screen name="Order List" component={BrokerOrderListScreen} 
                options={{
                    tabBarLabel: 'Orders'
                }} 
                />
            </androidTab.Navigator>
        )
        
    }
}

export default BrokerTabs