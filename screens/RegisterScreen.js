import React, { useState, useEffect }  from 'react'
import { StyleSheet, TextInput, Text, View, ScrollView, TouchableOpacity,Button, Platform, Modal, KeyboardAvoidingView} from 'react-native';
import { PrimaryBlue, PrimaryBlueOpacity } from '../color'
import LogoImage from '../components/LogoImage';
import Spinner from 'react-native-loading-spinner-overlay';
import DateTimePicker from '@react-native-community/datetimepicker'

const RegisterScreen = ({navigation, route}) => {

    useEffect(() => {
        if(route.params?.role){ setRoleSelect(route.params.role) }
    })
    
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [name, setName] = useState('')
    const [dob, setDob] = useState('')
    const [roleSelect, setRoleSelect] = useState('')
    const [loading, setLoading] = useState(false)
    //datepicker
    const [date, setDate] = useState(new Date());
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;

        setShow(Platform.OS === 'ios'); // has to be on first line.
        setDate(currentDate);
        setDob(currentDate.toISOString().slice(0,10))

        
    };

    const showMode = currentMode => {
        setShow(show => !show);
        setMode(currentMode);
    };

    const showDatepicker = () => {
        showMode('date');
    };

    const continueHandler = () => {
        console.log("sign, email: ", email, " password: ", password, "role:", roleSelect);
        // Check validation, email existed etc.
        // 
        setLoading(true)
        setTimeout(()=> {
            setLoading(false)
            navigation.navigate(`${roleSelect} More Info`, {email, password, roleSelect, name, dob})
        }, 2000)
        // if true => navigation.navigate('more info')
        // navigation.navigate("More Info", {email, password, roleSelect, name, dob})
        // else => stay, pop error message
    }
    const nameChangeHandler = (enteredName) => {
        setName(enteredName)   
    }

    const dobChangeHandler = (enteredDob) => {
        setDob(enteredDob)   
    }

    const emailChangeHandler = (enteredEmail) => {
        setEmail(enteredEmail)   
    }

    const passwordChangeHandler = (enteredPassword) => {
        setPassword(enteredPassword)
    }

    const toggleAgreementOpen = () => {
        // open agreement modal
        setagreementOpen(open => !open)
    }

    const iAgreeHandler = () => {
        toggleAgreementOpen()
        setHaveAgreed(agree => !agree)
    }

    return (
        <KeyboardAvoidingView style={styles.screenContainer} behavior="padding">
            <Spinner
                visible={loading}
                overlayColor={'rgba(0,0,0,0.5)'}
                textContent={'loading...'}
                textStyle={styles.spinnerTextStyle}
            />
            <View style={styles.contentContainer} >
                <View style={styles.titleContainer}>
                    <LogoImage />
                    <Text style={styles.textDis}>Register</Text>
                </View>
                <View >
                    <Text style={{color: 'rgba(255,255,255,0.7)', fontSize: 14}}>select your role</Text>
                </View>
                <View style={styles.roleBtnContainer}>
                    <TouchableOpacity onPress={()=> {setRoleSelect('Borrower')}}>
                        <View style={roleSelect === 'Borrower'? styles.rolePressedBtn : styles.roleBtn}>
                            <Text style={styles.btnText}>Borrower</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=> {setRoleSelect('Broker')}}>
                        <View style={roleSelect === 'Broker'? styles.rolePressedBtn : styles.roleBtn}>
                            <Text style={styles.btnText}>Broker</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=> {setRoleSelect('Lender')}}>
                        <View style={roleSelect === 'Lender'? styles.rolePressedBtn : styles.roleBtn}>
                            <Text style={styles.btnText}>Lender</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.inputContainer}>
                    <TextInput 
                    placeholder="Full Name" 
                    textContentType='name'
                    autoCorrect={false} 
                    autoCapitalize="words" 
                    placeholderTextColor='rgba(255,255,255,0.7)' 
                    onChangeText={nameChangeHandler} 
                    value={name}
                    style={styles.textInput} />
                    <TouchableOpacity onPress={showDatepicker} style={styles.dateInput} >
                        <TextInput 
                        editable={false}
                        keyboardType="number-pad"
                        placeholder="Date of Birth        " 
                        autoCorrect={false} 
                        placeholderTextColor='rgba(255,255,255,0.7)' 
                        value={dob}
                        style={styles.showDateDis}/>
                        <TouchableOpacity style={styles.showDatePickerBtn} onPress={showDatepicker}>
                            <View style={{}}>
                                <Text style={styles.btnText, {color: 'white'}}>▼</Text>
                            </View>
                        </TouchableOpacity>
                    </TouchableOpacity>
                    
                    <TextInput 
                    placeholder="Email" 
                    keyboardType='email-address' 
                    textContentType='emailAddress'
                    autoCorrect={false} 
                    autoCompleteType='email' 
                    autoCapitalize="none" 
                    placeholderTextColor='rgba(255,255,255,0.7)' 
                    onChangeText={emailChangeHandler} 
                    value={email} style={styles.textInput} />
                    <TextInput 
                    placeholder="Password" 
                    autoCorrect={false} 
                    autoCompleteType='password' 
                    secureTextEntry={true}
                    textContentType="password"
                    autoCapitalize="none" 
                    placeholderTextColor='rgba(255,255,255,0.7)' 
                    onChangeText={passwordChangeHandler} 
                    value={password} 
                    style={styles.textInput} />
                </View>
                <View style={styles.registerContainer}>
                    <TouchableOpacity onPress={continueHandler}>
                        <View style={styles.registerBtn}>
                            <Text style={styles.btnText}>CONTINUE</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
            {show && (
                <View style={styles.datePickerContainer}>
                    {Platform.OS === 'ios' && (
                        <View style={styles.closeDatePickerBtnContainer}>
                            <TouchableOpacity style={styles.closeDatePickerBtn} onPress={showDatepicker}>
                                <View style={{}}>
                                    <Text style={styles.btnText, {color: 'white'}}>Done</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    )}
                    <DateTimePicker
                    testID="dateTimePicker"
                    timeZoneOffsetInMinutes={0}
                    value={date}
                    mode={mode}
                    is24Hour={true}
                    display="default"
                    onChange={onChange}
                    />
                </View>
                
            )}
        </KeyboardAvoidingView>
    )
}

const styles = StyleSheet.create({
    screenContainer: {
        flex: 1,
        paddingTop: 50,
        backgroundColor: PrimaryBlue
    },
    backgroundImageWrapper:{
        flex: 1,
    },
    contentContainer: {
        flex: 1,
        justifyContent: "space-between",
        alignItems: 'center',
        // borderWidth: 1,
        // borderColor: 'black'
    },
    titleContainer: {
        flex: 2,
        // borderWidth: 1,
        // borderColor: 'black',
        justifyContent: 'space-around',
        alignItems: "center"
    },
    textDis: {
        textAlign: 'center',
        color: "white",
        fontSize: 26
    },
    roleBtnContainer: {
        flex: 0.6,
        marginBottom: 10,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems:"center",
        width: "90%",
    },
    roleBtn: {
        backgroundColor: 'rgba(255,255,255, 0.65)',
        borderRadius: 5,
        height: 50,
        width: 100,
        justifyContent: "center"
    },
    rolePressedBtn: {
        backgroundColor: 'rgba(255,255,255, 1)',
        borderRadius: 5,
        height: 50,
        width: 100,
        justifyContent: "center"
    },
    inputContainer: {
        flex: 2,
        width: '80%',
        justifyContent: "space-around",
        alignItems: "center",
        // borderWidth: 1,
        // borderColor: 'black',
        
    },
    textInput: { 
        width: "80%", 
        padding: 10,
        textAlign: "center",
        borderBottomColor: 'rgba(255,255,255,0.3)',
        borderBottomWidth: 1,
        // borderWidth: 1,
        // borderColor: 'black',
    },
    dateInput: {
        width: "80%", 
        flexDirection: 'row',
        padding: 10,
        justifyContent: 'center',
        borderBottomColor: 'rgba(255,255,255,0.3)',
        borderBottomWidth: 1,

        // borderWidth: 1,
        // borderColor: 'black',
    },
    showDateDis: {
        flex: 15,
        textAlign: "center"
    },
    showDatePickerBtn: {
        flex: 1
    },
    registerContainer: {
        flex: 1,
        width: '80%',
        // borderWidth: 1,
        // borderColor: 'black',
        justifyContent: 'flex-start'
    },
    registerBtn: {
        backgroundColor: 'white',
        borderRadius: 5,
        height: 50,
        width: '100%',
        justifyContent: 'center',
    },
    agreementContainer: {
        flex: 0.5,
        width: '80%',
        flexDirection: 'row',
        // borderWidth: 1,
        // borderColor: 'black',
        justifyContent: 'center',
        alignItems:"center"
    },
    agreementOpenBtn: {
        backgroundColor: '#00000000',
        borderRadius: 5,
        height: 50,
        width: '100%',
        justifyContent: 'center',
    },
    agreementBtnText :{
        textAlign: "center",
        color: 'white',
        textDecorationLine: "underline"
    },
    btnText: {
        textAlign: "center",
        color: PrimaryBlue
    },
    agreementModelContainer: {
        flex: 1,
        paddingTop: 50,
        justifyContent: "center",
        alignItems: "center",
        borderWidth: 1,
        borderColor: 'black',
    },
    agreementContentContainer: {
        flex: 1,
        width: '85%',
        height: 1000,
        marginBottom: 20,
        borderWidth: 1,
        borderColor: '#999999',
        
    },
    agreementBtnContainer: {
        // flex: 1,
        height: 100,
        flexDirection: "row",
        justifyContent: "space-around",
        // borderWidth: 1,
        // borderColor: 'black',
        width: '80%'
    },
    agreementBtn: {
        borderColor: PrimaryBlue,
        borderWidth: 1,
        borderRadius: 5,
        height: 40,
        width: 100,
        justifyContent: "center"
    }, 
    closeDatePickerBtnContainer: {
        alignItems: 'flex-end',
    },
    closeDatePickerBtn: {
        // borderWidth: 1,
        // borderColor: 'black',
        paddingRight: '5%'
    },
    spinnerTextStyle: {
        color: '#FFF'
    },

})

export default RegisterScreen