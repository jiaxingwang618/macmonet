import React from 'react'
import { StyleSheet, TextInput, Text, View, Button, ScrollView, FlatList, TouchableOpacity, Image, ImageBackground } from 'react-native';
import { PrimaryBlue, PrimaryBlueOpacity } from '../color'
import LogoImage from '../components/LogoImage';

const SelectScreen = ({navigation}) => {
    return (
        <View style={styles.screenContainer} >
            <ImageBackground style={styles.backgroundImageWrapper} source={require('../assets/bgiTRT.jpeg')} style={{width: '100%', height: '100%'}}>
                <View style={styles.contentContainer}>
                    <View style={styles.logoContainer}>
                        <LogoImage />
                    </View>
                    <View style={styles.textContainer}><Text style={styles.textDis}>Sign In As...</Text></View>
                    {/* <Button title="go to goalist" onPress={()=> navigation.navigate('Goal List', { param1: 'param1', param2: "param2"})}></Button> */}
                    <View style={styles.roleBtnContainer}>
                        <TouchableOpacity onPress={()=> navigation.navigate('Log In', { role: 'Borrower'})}>
                            <View style={styles.roleBtn}>
                                <Text style={styles.btnText}>Borrower</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=> navigation.navigate('Log In', { role: 'Broker'})}>
                            <View style={styles.roleBtn}>
                                <Text style={styles.btnText}>Broker</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=> navigation.navigate('Log In', { role: 'Lender'})}>
                            <View style={styles.roleBtn}>
                                <Text style={styles.btnText}>Lender</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.registerBtnContainer}>
                        <TouchableOpacity onPress={()=> navigation.navigate('Register')}>
                            <View style={styles.registerBtn}>
                                <Text style={styles.btnText}>REGISTER</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                
            </ImageBackground>
        </View>
    )
}

const styles = StyleSheet.create({
    screenContainer: {
        flex: 1,
    },
    backgroundImageWrapper:{
        flex: 1,
    },
    contentContainer: {
        flex: 1,
        paddingTop: 50,
        justifyContent: "space-between",
        alignItems: 'center',
        backgroundColor: PrimaryBlueOpacity
    },
    textContainer: {
        flex: 1,
        // borderWidth: 1,
        // borderColor: 'black',
        justifyContent: 'center'
    },
    textDis: {
        textAlign: 'center',
        color: "white",
        fontSize: 24
    },
    logoContainer: {
        flex: 4,
        // borderWidth: 1,
        // borderColor: "black",
        justifyContent: 'center'
    },
    roleBtnContainer: {
        flex: 2,
        marginBottom: 10,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        width: "90%",
    },
    roleBtn: {
        backgroundColor: 'rgba(255,255,255, 0.75)',
        borderRadius: 5,
        height: 50,
        width: 100,
        justifyContent: "center"
    },
    registerBtnContainer: {
        marginTop: 20,
        flex: 2,
        width: "80%",
    },
    registerBtn: {
        backgroundColor: 'white',
        borderRadius: 5,
        height: 50,
        justifyContent: 'center'
    },

    btnText: {
        textAlign: "center",
        color: PrimaryBlue
    }
})

export default SelectScreen