import React, { useState, useEffect } from "react"
import { View, Text, StyleSheet, Image, Dimensions} from "react-native"
import { MoreInfoScreenStyleSheet as styles} from '../../apiHelper/predefinedStyleSheet'
import TitleHeader from '../../components/TitleHeader'
import {
    LineChart,
    BarChart,
    PieChart,
    ProgressChart,
    ContributionGraph,
    StackedBarChart
  } from 'react-native-chart-kit'
import { PrimaryBlue, PrimaryOrange } from "../../color"
import MoneyAmount from "../../components/MoneyAmount"
import WithdrawDepositButtons from "../../components/WithdrawDepositButtons"
import LinkCopy from "../../components/LinkCopy"

const LenderCapitalScreen = () => {
    const [pieChartData, setPieChartData] = useState([
        {
            name: "In Use",
            population: 20000.00,
            color: PrimaryOrange,
            legendFontColor: "black",
            legendFontSize: 15
          },
          {
            name: "Free",
            population: 30000.00,
            color: PrimaryBlue,
            legendFontColor: "black",
            legendFontSize: 15
          },
          
    ])

    const [moneyInAccount, setMoneyInAccount] = useState(0)
    const [link, setLink] = useState('testLink1234563y2tfeqwv23g4efqweb342gqfv2')

    useEffect(()=> {
        setMoneyInAccount(20893.45)
    })

    useEffect(() => {
        setLink('newTestlink213rvgtb432143grefvwr23g4rwevadsdfadsfasdfasf')
    })

    const chartConfig = {
        backgroundGradientFrom: "#1E2923",
        backgroundGradientFromOpacity: 0,
        backgroundGradientTo: "#08130D",
        backgroundGradientToOpacity: 0.5,
        color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
      };


    return (
        <View style={{...styles.screenContainer, ...localStyle.screenContainer}} >
            <TitleHeader title="My Capital" />
            <View style={localStyle.amountContainer}>
                <MoneyAmount amount={moneyInAccount}/>
            </View>
            <View style={localStyle.profilePicContainer}>
                <Image style={localStyle.profilePic} source={require('../../assets/boypp.jpg')} />
            </View>
            <View style={localStyle.pieChartContainer} >
                <PieChart
                data={pieChartData}
                width={Dimensions.get("screen").width}
                height={200}
                chartConfig={chartConfig}
                accessor="population"
                backgroundColor="transparent"
                paddingLeft="0"
                absolute
                />
            </View>
            <View style={localStyle.linkContainer} >
                <LinkCopy link={link} />
            </View>
            <View style={localStyle.btnContainer} >
                <WithdrawDepositButtons />
            </View>
        </View>
    )
}

export default LenderCapitalScreen

const localStyle = StyleSheet.create({
    screenContainer: {
        backgroundColor: 'white',
    },
    amountContainer: {
        flex: 0.7,
        // borderWidth: 1,
        // borderColor: 'black',
        justifyContent: 'center',
        alignItems: "center"
    },
    profilePicContainer: {
        flex: 1,
        // borderWidth: 1,
        // borderColor: 'black',
        alignItems: "center",
        justifyContent: "center"
    },
    profilePic: {
        width: 60,
        height: 60,
        borderRadius: 50
    },
    pieChartContainer: {
        flex: 2,
        // borderWidth: 1,
        // borderColor: 'black',
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        padding: 0
    },
    linkContainer: {
        flex: 0.5,
        // borderWidth: 1,
        // borderColor: 'black',
    },
    btnContainer: {
        flex: 1,
        // borderWidth: 1,
        // borderColor: 'black',
    }

})