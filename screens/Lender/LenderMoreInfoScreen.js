import React, { useState, useEffect }  from 'react'
import { StyleSheet, TextInput, Text, View, ScrollView, TouchableOpacity,Button, Platform, Modal, Alert, KeyboardAvoidingView } from 'react-native';
import { PrimaryBlue, PrimaryBlueOpacity } from '../../color'
import LogoImage from '../../components/LogoImage';
import { onlyNumber } from '../../apiHelper/helper';
import Spinner from 'react-native-loading-spinner-overlay';
import { MoreInfoScreenStyleSheet as styles} from '../../apiHelper/predefinedStyleSheet'


const LenderMoreInfoScreen = ({navigation, route}) => {
    const {email, password, dob, name, roleSelect} = route.params
    
    const [yearExp, setYearExp] = useState('')
    const [agreementOpen, setagreementOpen] = useState(false)
    const [haveAgreed, setHaveAgreed] = useState(false)
    const [loading, setLoading] = useState(false)


    const registerHandler = () => {
        // check agreement agreed
        if(!haveAgreed) { return Alert.alert(
            'Agreement not agreed',
            'In order to continue, you must agree to our Term of Service',
            [
                {text: 'OK', onPress: () => {console.log("ok")}}
            ],
            {cancelable: false}
        )}
        
        console.log("sign, email: ", email, " password: ", password, "role:", roleSelect);
        // Check validation
        setLoading(true)
        setTimeout(()=> {
            setLoading(false)
            navigation.navigate("Select Role")
        }, 2000)
        // try register
        // if true => navigation.navigate('sign in page')
        // navigation.navigate("Select Role")
        // else => stay, pop error message
    }

    const yearExpChangeHandler = (yearExp) => {
        setYearExp(onlyNumber(yearExp))   
    }

    const toggleAgreementOpen = () => {
        // open agreement modal
        setagreementOpen(open => !open)
    }

    const iAgreeHandler = () => {
        toggleAgreementOpen()
        setHaveAgreed(agree => !agree)
    }

    return (
        <KeyboardAvoidingView style={styles.screenContainer} behavior="padding">
            <Spinner
                visible={loading}
                overlayColor={'rgba(0,0,0,0.5)'}
                textContent={'loading...'}
                textStyle={styles.spinnerTextStyle}
            />
            <View style={styles.contentContainer} >
                <View style={styles.titleContainer}>
                    <LogoImage />
                    <Text style={styles.textDis}>Lender More Info</Text>
                </View>
                <View style={styles.inputContainer}>
                    <TextInput 
                    placeholder="Years of Investing Experience" 
                    keyboardType='number-pad' 
                    autoCorrect={false} 
                    autoCapitalize="none" 
                    placeholderTextColor='rgba(255,255,255,0.7)' 
                    onChangeText={yearExpChangeHandler} 
                    returnKeyType={ 'done' }
                    value={yearExp} style={styles.textInput} />

                </View>
                <View style={styles.agreementContainer}>
                    <Text>
                        {haveAgreed ? 
                        <Text style={{fontSize: 20, color: 'green', fontWeight: "bold"}}>✓</Text> : 
                        <Text style={{fontSize: 20, color: 'red', fontWeight: "bold"}}>☐</Text> }
                    </Text>
                    <TouchableOpacity onPress={toggleAgreementOpen}>
                        <View style={styles.agreementOpenBtn}>
                            <Text style={styles.agreementBtnText}>Please read and agree the contract</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.registerContainer}>
                    <TouchableOpacity onPress={registerHandler}>
                        <View style={styles.registerBtn}>
                            <Text style={styles.btnText}>REGISTER</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
            
            <Modal visible={agreementOpen} animationType='slide'>
                <View style={styles.agreementModelContainer}> 
                    <ScrollView style={styles.agreementContentContainer}>
                        <Text>
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                            A very long document A very long document A very long document A very long document A very long document A very long document A very long document 
                        </Text>
                    </ScrollView>
                    <View style={styles.agreementBtnContainer}>
                        <TouchableOpacity onPress={toggleAgreementOpen}>
                            <View style={styles.agreementBtn}>
                                <Text style={styles.btnText}>Back</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={iAgreeHandler}>
                            <View style={styles.agreementBtn}>
                            <Text style={styles.btnText}>{haveAgreed? 'I Disagree' : 'I Agree'}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        </KeyboardAvoidingView>
    )
}

export default LenderMoreInfoScreen