import React from 'react'
import { createDrawerNavigator } from '@react-navigation/drawer';
import LenderCapitalScreen from './LenderCapitalScreen'
import SettingMenuScreen from '../SettingMenuScreen';

const Drawer = createDrawerNavigator();

const LenderDrawer = props => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="My Capital" component={LenderCapitalScreen} />
      <Drawer.Screen name="Setting Menu" component={SettingMenuScreen} />
    </Drawer.Navigator>
  );
}

export default LenderDrawer


