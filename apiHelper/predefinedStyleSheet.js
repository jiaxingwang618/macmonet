import { StyleSheet} from 'react-native'
import {PrimaryBlue, PrimaryBlueOpacity} from '../color'

export const BrokerListScreenStyleSheet = StyleSheet.create({
    screenContainer: {
        flex: 1,
        paddingTop: 30,
        backgroundColor: 'white'
    },
})

export const MoreInfoScreenStyleSheet = StyleSheet.create({
    screenContainer: {
        flex: 1,
        paddingTop: 10,
        backgroundColor: PrimaryBlue
    },
    card: {
        width: '90%',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
        elevation: 9,
        backgroundColor: 'white',
    },
    backgroundImageWrapper:{
        flex: 1,
    },
    contentContainer: {
        flex: 1,
        justifyContent: "space-between",
        alignItems: 'center',
        // borderWidth: 1,
        // borderColor: 'black'
    },
    titleContainer: {
        flex: 2,
        // borderWidth: 1,
        // borderColor: 'black',
        justifyContent: 'space-around',
        alignItems: "center"
    },
    textDis: {
        textAlign: 'center',
        color: "white",
        fontSize: 26
    },
    roleBtnContainer: {
        flex: 0.6,
        marginBottom: 10,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems:"center",
        width: "90%",
    },
    roleBtn: {
        backgroundColor: 'rgba(255,255,255, 0.65)',
        borderRadius: 5,
        height: 50,
        width: 100,
        justifyContent: "center"
    },
    rolePressedBtn: {
        backgroundColor: 'rgba(255,255,255, 1)',
        borderRadius: 5,
        height: 50,
        width: 100,
        justifyContent: "center"
    },
    inputContainer: {
        flex: 2,
        width: '80%',
        justifyContent: "space-around",
        alignItems: "center",
        // borderWidth: 1,
        // borderColor: 'black',
        
    },
    textInput: { 
        width: "80%", 
        padding: 10,
        textAlign: "center",
        borderBottomColor: 'rgba(255,255,255,0.3)',
        borderBottomWidth: 1,
        // borderWidth: 1,
        // borderColor: 'black',
    },
    dateInput: {
        width: "80%", 
        flexDirection: 'row',
        padding: 10,
        justifyContent: 'center',
        borderBottomColor: 'rgba(255,255,255,0.3)',
        borderBottomWidth: 1,

        // borderWidth: 1,
        // borderColor: 'black',
    },
    showDateDis: {
        flex: 15,
        textAlign: "center"
    },
    showDatePickerBtn: {
        flex: 1
    },
    registerContainer: {
        flex: 1,
        width: '80%',
        // borderWidth: 1,
        // borderColor: 'black',
        justifyContent: 'flex-start'
    },
    registerBtn: {
        backgroundColor: 'white',
        borderRadius: 5,
        height: 50,
        width: '100%',
        justifyContent: 'center',
    },
    agreementContainer: {
        flex: 0.5,
        width: '80%',
        flexDirection: 'row',
        // borderWidth: 1,
        // borderColor: 'black',
        justifyContent: 'center',
        alignItems:"center"
    },
    agreementOpenBtn: {
        backgroundColor: '#00000000',
        borderRadius: 5,
        height: 50,
        width: '100%',
        justifyContent: 'center',
    },
    agreementBtnText :{
        textAlign: "center",
        color: 'white',
        textDecorationLine: "underline"
    },
    btnText: {
        textAlign: "center",
        color: PrimaryBlue
    },
    agreementModelContainer: {
        flex: 1,
        paddingTop: 50,
        justifyContent: "center",
        alignItems: "center",
        borderWidth: 1,
        borderColor: 'black',
    },
    agreementContentContainer: {
        flex: 1,
        width: '85%',
        height: 1000,
        marginBottom: 20,
        borderWidth: 1,
        borderColor: '#999999',
        
    },
    agreementBtnContainer: {
        // flex: 1,
        height: 100,
        flexDirection: "row",
        justifyContent: "space-around",
        // borderWidth: 1,
        // borderColor: 'black',
        width: '80%'
    },
    agreementBtn: {
        borderColor: PrimaryBlue,
        borderWidth: 1,
        borderRadius: 5,
        height: 40,
        width: 100,
        justifyContent: "center"
    }, 
    closeDatePickerBtnContainer: {
        alignItems: 'flex-end',
    },
    closeDatePickerBtn: {
        // borderWidth: 1,
        // borderColor: 'black',
        paddingRight: '5%'
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
})