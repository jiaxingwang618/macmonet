import React, { useState } from 'react';
import { StyleSheet, TextInput, Text, View, Button, ScrollView, FlatList } from 'react-native';
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import SelectRoleScreen from './screens/SelectRoleScreen'
import LogInScreen from './screens/LogInScreen'
import RegisterScreen from './screens/RegisterScreen'
import BorrowerMoreInfoScreen from './screens/Borrower/BorrowerMoreInfoScreen'
import BrokerMoreInfoScreen from './screens/Broker/BrokerMoreInfoScreen'
import LenderMoreInfoScreen from './screens/Lender/LenderMoreInfoScreen'
import BrokerDrawer from './screens/Broker/BrokerDrawer'
import LenderDrawer from './screens/Lender/LenderDrawer';
import BorrowerDrawer from './screens/Borrower/BorrowerDrawer';



export default function App() {
  
  const Stack = createStackNavigator();

  return (
    <NavigationContainer>
      <Stack.Navigator headerMode='none' initialRouteName="Select Role" 
      screenOptions={{
        headerStyle: {
          backgroundColor: '#f4511e',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold'
        },
      }}
      >
        <Stack.Screen name="Select Role" component={SelectRoleScreen} options={{ title: ''}} initialParams={{}} />
        <Stack.Screen name="Log In" component={LogInScreen} />
        <Stack.Screen name="Register" component={RegisterScreen} />

        <Stack.Screen name="Borrower More Info" component={BorrowerMoreInfoScreen} />
        <Stack.Screen name="Broker More Info" component={BrokerMoreInfoScreen} />
        <Stack.Screen name="Lender More Info" component={LenderMoreInfoScreen} />

        <Stack.Screen name="Borrower Home Page" component={BorrowerDrawer} />

        <Stack.Screen name="Broker Home Page" component={BrokerDrawer} />

        <Stack.Screen name="Lender Home Page" component={LenderDrawer} />
      </Stack.Navigator>
    </NavigationContainer>
    
  );
}

const styles = StyleSheet.create({
  screen: { padding: 50 }
});
