import React from "react"
import { View, Text, StyleSheet, Image, Dimensions} from "react-native"

const MoneyAmount = props => {
    return (
        <View style={localStyle.container}>
            <View style={localStyle.dollarSignContainer}>
                <Text style={localStyle.dollarSign} >$</Text>
            </View>
            <View style={localStyle.numberContainer}>
                <Text style={localStyle.number} >{props.amount ? props.amount : 9999.99}</Text>
            </View>
        </View>
    )
}

const localStyle = StyleSheet.create({
    container: {
        flexDirection: "row",
        justifyContent: 'center',
        // borderWidth: 1,
        // borderColor: 'black'
    },
    dollarSignContainer: {
        marginRight: 8,
        // justifyContent: 'flex-start',
        // alignItems: 'flex-start',
        // borderWidth: 1,
        // borderColor: 'black'
    },
    dollarSign : {
        fontSize: 25
    },
    numberContainer: {
        // borderWidth: 1,
        // borderColor: 'black'
    },
    number: {
        fontSize: 45
    }

})

export default MoneyAmount