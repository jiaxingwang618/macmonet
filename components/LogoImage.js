import React from 'react'
import{Image} from 'react-native'

const LogoImage = props => {
    const width = props.width? props.width : 50;
    const height = props.height? props.height: 50;
    return (<Image
    style={{...props.style, width, height }}
    source={require('../assets/logo.png')}
    />)
}

export default LogoImage