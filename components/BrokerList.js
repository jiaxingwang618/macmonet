import React, { useState } from 'react'
import {FlatList, View, Image, Text, StyleSheet, ScrollView} from 'react-native'
import BrokerListItem from './BrokerListItem'
const BrokerList = () => {
    const [data, setData] = useState([
        {
            name:'a',
            id: 1
        },
        {
            name: 'b',
            id: 2
        },
        {
            name: 'c',
            id: 3
        },
        {
            name: 'c',
            id: 4
        },
        {
            name: 'c',
            id: 5
        },
        {
            name: 'c',
            id: 6
        },
        {
            name: 'c',
            id: 7
        },
        {
            name: 'c',
            id: 8
        },
        {
            name: 'c',
            id: 9
        },
        {
            name: 'c',
            id: 10
        },
        {
            name: 'c',
            id: 11
        },
        {
            name: 'c',
            id: 12
        },
        {
            name: 'c',
            id: 13
        }
    ])
    return (
        <View style={LocalStyle.listContainer}>
            <FlatList  keyExtractor={(item, index) => item.id.toString()} data={data} renderItem={item => (
                <BrokerListItem user={item.item.name}/>
            
            )} />
        </View>
        
    )
}

export default BrokerList

const LocalStyle = StyleSheet.create({
    listContainer: {
        flex: 10,

        borderColor: '#bbbbbb',
        borderBottomWidth: 1,
        borderTopWidth: 1
        // borderWidth: 1,
        // borderColor: 'black'
    }
})