import React from "react"
import { View, Text, StyleSheet, Image, Dimensions} from "react-native"
import { TouchableOpacity } from "react-native-gesture-handler"
import { MoreInfoScreenStyleSheet as styles } from '../apiHelper/predefinedStyleSheet'

const WithdrawDepositButtons = props => {
    return (
        <View style={localStyle.container}>
            <View style={localStyle.withdrawContainer}>
                <View style={localStyle.withdrawButtonBorder}>
                    <TouchableOpacity style={localStyle.withdrawButton}>
                        <Text style={localStyle.withdrawText}>Withdraw</Text>
                    </TouchableOpacity>
                </View>
                
            </View>
            <View style={{width: 1}}>
                {/* seperator */}
            </View>
            <View style={localStyle.depositContainer}>
                <View style={localStyle.depositButtonBorder}>
                    <TouchableOpacity style={localStyle.depositButton}>
                        <Text style={localStyle.depositText}>Deposit</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

const localStyle = StyleSheet.create({
    container: {
        flexDirection: "row",
        justifyContent: 'center',
        flex: 1,
        alignItems: "center"
        // borderWidth: 1,
        // borderColor: 'black'
    },
    withdrawContainer: {
        flex:1,
        // borderWidth: 1,
        // borderColor: 'black',
        alignItems: "flex-end",
        justifyContent: "center",

    },
    withdrawButtonBorder: {
        borderTopLeftRadius: 20,
        borderBottomLeftRadius: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
        elevation: 5,
        backgroundColor: 'white',
    },
    withdrawButton: {
        height: 50,
        width: 180,
        borderTopLeftRadius: 20,
        borderBottomLeftRadius: 20,
        // borderColor: 'black',
        // borderWidth: 1,
        justifyContent: "center",
        alignItems: "center",
        
    },
    withdrawText: {
        fontSize: 16,
        color: '#3fd975'
    },
    depositContainer: {
        flex:1,
        // borderWidth: 1,
        // borderColor: 'black',
        alignItems: "flex-start",
        justifyContent: "center"
    },
    depositButtonBorder: {
        borderTopRightRadius: 20,
        borderBottomRightRadius: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
        elevation: 5,
        backgroundColor: 'white',
    },
    depositButton: {
        height: 50,
        width: 180,
        borderTopRightRadius: 20,
        borderBottomRightRadius: 20,
        // borderColor: 'black',
        // borderWidth: 1,
        justifyContent: "center",
        alignItems: "center",
        
    },
    depositText: {
        fontSize: 16,
        color: '#3fd975'
    },

})

export default WithdrawDepositButtons