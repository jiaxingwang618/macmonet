import React from "react";
import {View, TextInput, Button, StyleSheet, Modal} from 'react-native'

const GoalInput = props => {
    return (
        <Modal visible={props.modalOpen} animationType='slide'>
            <View style={styles.inputContainer}>
                <TextInput placeholder="Course Goal" onChangeText={props.goalInputHandler} value={props.enteredGoal} style={styles.goalInput} />
                <View style={styles.buttonView}>
                    <View style={styles.button}><Button title="Add" onPress={props.addGoalHandler} /></View>
                    <View style={styles.button}><Button color="red" title="Back" onPress={props.toggleModal} /></View>
                </View>
            </View>
        </Modal>
        
    )
}

const styles = StyleSheet.create({
    
    inputContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
      },
      goalInput: { width: "80%", borderBottomColor: 'black', borderBottomWidth: 1, padding: 10, marginBottom: 10 },
      buttonView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: "center",
        width: "50%"
      },
      button: {
          width: 100
      }
  });


export default GoalInput