import React from "react"
import { View, Text, StyleSheet, Image, Dimensions, TouchableOpacity} from "react-native"
import { PrimaryOrange } from "../color"

const LinkCopy = props => {
    return (
        <View style={localStyle.container}>
            <View style={localStyle.linkContainer}>
                <Text numberOfLines={1} ellipsizeMode='middle' >{props.link}</Text>
            </View>
            <View style={localStyle.copyBtnContainer}>
                <TouchableOpacity>
                    <Text style={localStyle.copyBtnText}>Copy</Text>
                </TouchableOpacity>
            </View>
            <View style={localStyle.myInfoContainer}>
                <TouchableOpacity>
                    <Text style={localStyle.infoBtnText} >My Info</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const localStyle = StyleSheet.create({
    container: {
        flexDirection: "row",
        justifyContent: 'center',
        alignItems: "center",
        flex: 1,
        // borderWidth: 1,
        // borderColor: 'black'
    },
    linkContainer: {
        flex: 7,
        flexDirection: "row",
        paddingLeft: 10
    },
    copyBtnContainer: {
        flex: 1.5,
        paddingLeft: 5
    },
    copyBtnText: {
        color: PrimaryOrange
    },
    myInfoContainer: {
        flex: 1.5,
        paddingRight: 5
    },
    infoBtnText: {
        color: PrimaryOrange
    }

})

export default LinkCopy