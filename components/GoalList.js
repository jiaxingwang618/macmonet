import React from "react";
import {View, Text, FlatList, StyleSheet, TouchableOpacity} from 'react-native'

const GoalList = props => {
    const {route, navigation} = props
    console.log(route.params.param1);
    console.log(route.params.param2);
    console.log(route.params.name);
    
    // navigation.setParams();
    // navigation.setOptions({title: 'updated'})
    
    return (
        <FlatList keyExtractor={(item, index) => item.id} data={props.goals} renderItem={goal => (
            <TouchableOpacity onPress={() => props.onDelete(goal.item.id)}>
                <View style={styles.goalItem}>
                    <Text >{goal.item.val}</Text>
                </View>
            </TouchableOpacity>
          
        )} />
    )
}

const styles = StyleSheet.create({
    goalItem: {
      padding: 10,
      marginTop: 10,
      backgroundColor:'#ccc',
      borderColor: 'black',
      borderWidth: 1,
      
    }
  });

export default GoalList