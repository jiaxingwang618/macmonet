import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity} from 'react-native'
import LogoImage from "./LogoImage";
import { Button } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';
import { PrimaryBlue } from '../color';

const TitleHeader = props => {
    const navigation = useNavigation()
    return (
        <View style={{...localStyle.headerContainer,}}>
            <View style={localStyle.btnContainer}>
                <TouchableOpacity onPress={()=> navigation.toggleDrawer() }>
                    <View style={localStyle.drawerBtnView} >
                        <Text style={localStyle.drawerBtnText}>
                            >
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={localStyle.titleContainer}>
                <LogoImage style={localStyle.logoImage} height={35} width={35} />
                <Text style={localStyle.titleText}>{props.title}</Text>
            </View>
            <View style={localStyle.spaceContainer}>
                {/* for styling add spacing */}
            </View>
        </View>
    )
}

const localStyle = StyleSheet.create({
    headerContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: "center"
    },
    btnContainer: {
        flex: 1,
        padding: 10
    },
    titleContainer: {
        flex: 12,
        flexDirection: "row",
        justifyContent: 'center'
    },
    spaceContainer: {
        flex: 1
    },
    logoImage: {
        marginRight: 10
    },
    titleText: {
        fontSize: 25
    },
    drawerBtnView: {
        marginLeft: '7%',
        marginRight: '20%',
        borderWidth: 1,
        width: 30,
        height: 30,
        justifyContent: "center",
        alignItems: "center",
        borderColor: PrimaryBlue,
        textAlign: "center"
    },
    drawerBtnText: {
        fontSize: 20,
        textAlignVertical: "center",
        color: PrimaryBlue
    }
})

export default TitleHeader