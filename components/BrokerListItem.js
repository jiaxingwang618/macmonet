import React from 'react'
import {FlatList, View, Image, Text, StyleSheet, ScrollView} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';

const BrokerListItem = props => {
    console.log("item", props.user);
    
    return (
        <View style={localStyle.ItemBox}>
            <View style={localStyle.imageColumn}>
                <Image style={localStyle.profilePic} source={require('../assets/boypp.jpg')} />
            </View>
            <View style={localStyle.InfoColumn}>
                <View style={localStyle.mainRow}>
                    <Text style={localStyle.nameText} >Tom Cruise</Text>
                    <Text style={localStyle.amountText} >$99,999,999</Text>
                </View>
                <View style={localStyle.secondRow}>
                    <Text style={localStyle.noteText} >Free-lance Developer</Text>
                    <Text style={localStyle.timeAgoText} >6 Days</Text>
                </View>
                <View style={localStyle.subTitleRow}>
                    <Text style={localStyle.greyText} >{new Date().toDateString()}</Text>
                    <TouchableOpacity><Text>。。。</Text></TouchableOpacity>
                </View>
            </View>
            <View style={localStyle.loadingContainer}>

            </View>
        </View>
    )
}

const localStyle = StyleSheet.create({
    ItemBox: {
        flex: 1,
        borderColor: '#bbbbbb',
        borderBottomWidth: 1,
        // borderTopWidth: 1,
        flexDirection: 'row',
        padding: 10,
        
    },
    imageColumn: {
        flex: 1,
        justifyContent: "center",
    },
    InfoColumn: {
        flex: 4,
    },
    mainRow: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    secondRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    noteText: {
        fontSize: 13
    },
    timeAgoText: {
        fontSize: 13
    },
    subTitleRow: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    greyText: {
        fontSize: 12,
        color: '#aaaaaa'
    },

    profilePic: {
        width: 50,
        height: 50,
        borderRadius: 8
    }
})

export default BrokerListItem